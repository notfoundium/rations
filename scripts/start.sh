#!/usr/bin/env bash

set -e

upgrade
exec poetry run uvicorn --reload --proxy-headers --host 0.0.0.0 --port 8000 src.main:app
