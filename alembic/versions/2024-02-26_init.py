"""init

Revision ID: 624a7f7cb9f6
Revises: 
Create Date: 2024-02-26 15:21:56.845480

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '624a7f7cb9f6'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('allergens',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('name', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('users',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('email', sa.String(), nullable=False),
    sa.Column('hashed_password', sa.String(), nullable=False),
    sa.Column('is_superuser', sa.Boolean(), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email')
    )
    op.create_table('meals',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('description', sa.String(), nullable=False),
    sa.Column('calories', sa.Float(), nullable=False),
    sa.Column('protein', sa.Float(), nullable=False),
    sa.Column('fat', sa.Float(), nullable=False),
    sa.Column('carbohydrate', sa.Float(), nullable=False),
    sa.Column('author_id', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['author_id'], ['users.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('programs',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('description', sa.String(), nullable=False),
    sa.Column('author_id', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['author_id'], ['users.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('rations',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('description', sa.String(), nullable=False),
    sa.Column('calories', sa.Float(), nullable=False),
    sa.Column('protein', sa.Float(), nullable=False),
    sa.Column('fat', sa.Float(), nullable=False),
    sa.Column('carbohydrate', sa.Float(), nullable=False),
    sa.Column('author_id', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['author_id'], ['users.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('sessions',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('user_id', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user_allergens',
    sa.Column('user_id', sa.String(), nullable=True),
    sa.Column('allergen_id', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['allergen_id'], ['allergens.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], )
    )
    op.create_table('dailies',
    sa.Column('id', sa.String(), nullable=False),
    sa.Column('weekday', sa.String(), nullable=False),
    sa.Column('ration_id', sa.String(), nullable=False),
    sa.Column('author_id', sa.String(), nullable=False),
    sa.ForeignKeyConstraint(['author_id'], ['users.id'], ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['ration_id'], ['rations.id'], ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('meal_allergens',
    sa.Column('meal_id', sa.String(), nullable=True),
    sa.Column('allergen_id', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['allergen_id'], ['allergens.id'], ),
    sa.ForeignKeyConstraint(['meal_id'], ['meals.id'], )
    )
    op.create_table('meal_ingredients',
    sa.Column('meal_id', sa.String(), nullable=True),
    sa.Column('ingredient_id', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['ingredient_id'], ['meals.id'], ),
    sa.ForeignKeyConstraint(['meal_id'], ['meals.id'], )
    )
    op.create_table('ration_meals',
    sa.Column('ration_id', sa.String(), nullable=True),
    sa.Column('meal_id', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['meal_id'], ['meals.id'], ),
    sa.ForeignKeyConstraint(['ration_id'], ['rations.id'], )
    )
    op.create_table('program_dailies',
    sa.Column('program_id', sa.String(), nullable=True),
    sa.Column('daily_id', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['daily_id'], ['dailies.id'], ),
    sa.ForeignKeyConstraint(['program_id'], ['programs.id'], )
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('program_dailies')
    op.drop_table('ration_meals')
    op.drop_table('meal_ingredients')
    op.drop_table('meal_allergens')
    op.drop_table('dailies')
    op.drop_table('user_allergens')
    op.drop_table('sessions')
    op.drop_table('rations')
    op.drop_table('programs')
    op.drop_table('meals')
    op.drop_table('users')
    op.drop_table('allergens')
    # ### end Alembic commands ###
