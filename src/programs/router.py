from fastapi import Depends, HTTPException, APIRouter, status
from sqlalchemy import select, or_
from sqlalchemy.exc import NoResultFound

from src.database import get_session, AsyncSession

from src.users.dependencies import current_user
from src.users.models import User

from .dependencies import program_author
from .models import Program, Daily
from .schemas import DailyCreate, DailyRead, ProgramCreate, ProgramRead


router = APIRouter()


@router.post(
    "/dailies",
    status_code=status.HTTP_201_CREATED,
    response_model=DailyRead
)
async def create_daily(
        create_data: DailyCreate,
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    create_data_dict = create_data.model_dump()
    create_data_dict["author_id"] = user.id
    daily = Daily(**create_data_dict)
    session.add(daily)
    await session.commit()
    await session.refresh(daily)
    return daily


@router.get(
    "/dailies",
    status_code=status.HTTP_200_OK,
    response_model=list[DailyRead]
)
async def get_my_dailies(
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    stmt = select(Daily).where(Daily.author_id == user.id)
    result = await session.scalars(stmt)
    return result.unique().all()


@router.post(
    "/programs",
    status_code=status.HTTP_201_CREATED,
    response_model=ProgramRead,
    dependencies=[
        Depends(current_user)
    ]
)
async def create_program(
        create_data: ProgramCreate,
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    create_data_dict = create_data.model_dump()
    create_data_dict["author_id"] = user.id
    program = Program(**create_data_dict)
    session.add(program)
    await session.commit()
    await session.refresh(program)
    return program


@router.get(
    "/programs",
    status_code=status.HTTP_200_OK,
    response_model=list[ProgramRead]
)
async def get_my_rations(
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    stmt = select(Program).where(Program.author_id == user.id)
    result = await session.scalars(stmt)
    return result.unique().all()


@router.patch(
    "/programs/{program_id}/add/{daily_id}",
    status_code=status.HTTP_200_OK,
    response_model=ProgramRead,
    dependencies=[Depends(program_author)]
)
async def add_daily(
        program_id: str,
        daily_id: str,
        session: AsyncSession = Depends(get_session)
):
    try:
        stmt = select(Program).where(Program.id == program_id)
        result = await session.execute(stmt)
        program = result.unique().scalar_one()
        stmt = select(Daily).where(Daily.id == daily_id)
        result = await session.execute(stmt)
        daily = result.unique().scalar_one()
    except NoResultFound:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Invalid Program or Daily ID")
    program.dailies.append(daily)
    session.add(program)
    await session.commit()
    await session.refresh(program)
    return program


@router.get(
    "/programs/search",
    status_code=status.HTTP_200_OK,
    response_model=list[ProgramRead]
)
async def search_program(
        q: str,
        session: AsyncSession = Depends(get_session)
):
    stmt = select(Program).where(or_(Program.name.ilike(f"%{q}%"), Program.description.ilike(f"%{q}%")))
    result = await session.execute(stmt)
    return result.unique().scalars()
