from pydantic import BaseModel, ConfigDict

from .constants import Weekday


class DailyCreate(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    ration_id: str
    weekday: Weekday


class DailyRead(DailyCreate):
    id: str
    author_id: str


class ProgramCreate(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    name: str
    description: str = ""


class ProgramRead(ProgramCreate):
    id: str
    author_id: str

    dailies: list[DailyRead]
