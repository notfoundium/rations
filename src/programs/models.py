from sqlalchemy import String, Column, ForeignKey, Table
from sqlalchemy.orm import relationship

from src.database import Base
from src.utils import generate_id


program_dailies = Table(
    "program_dailies",
    Base.metadata,
    Column("program_id", ForeignKey("programs.id")),
    Column("daily_id", ForeignKey("dailies.id")),
)


class Daily(Base):
    __tablename__ = "dailies"

    id = Column("id", String, nullable=False, primary_key=True, default=generate_id)
    weekday = Column("weekday", String, nullable=False)
    ration_id = Column("ration_id", ForeignKey("rations.id", ondelete="CASCADE"), nullable=False)
    author_id = Column("author_id", ForeignKey("users.id", ondelete="CASCADE"), nullable=False)


class Program(Base):
    __tablename__ = "programs"

    id = Column("id", String, nullable=False, primary_key=True, default=generate_id)
    name = Column("name", String, nullable=False)
    description = Column("description", String, nullable=False, default="")
    author_id = Column("author_id", ForeignKey("users.id", ondelete="CASCADE"), nullable=False)

    dailies = relationship(Daily, secondary=program_dailies, lazy="selectin")
