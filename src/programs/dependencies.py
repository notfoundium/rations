from fastapi import Depends, HTTPException, status
from sqlalchemy import select, and_
from sqlalchemy.exc import NoResultFound

from src.database import get_session, AsyncSession

from src.users.models import User
from src.users.dependencies import current_user

from .models import Program


async def program_author(
        program_id: str,
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    try:
        stmt = select(Program).where(and_(Program.id == program_id, Program.author_id == user.id))
        result = await session.execute(stmt)
        user = result.unique().scalar_one()
        return user
    except NoResultFound:
        raise HTTPException(status.HTTP_403_FORBIDDEN)
