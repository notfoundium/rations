from fastapi import Depends, HTTPException, status
from sqlalchemy import select, and_
from sqlalchemy.exc import NoResultFound

from src.database import get_session, AsyncSession

from src.users.models import User
from src.users.dependencies import current_user

from .models import Meal, Allergen


async def valid_allergen(
        allergen_name: str,
        session: AsyncSession = Depends(get_session)
):
    try:
        result = await session.execute(select(Allergen).where(Allergen.name == allergen_name))
        return result.unique().scalar_one()
    except NoResultFound:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Invalid allergen")


async def meal_author(
        meal_id: str,
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    try:
        stmt = select(Meal).where(and_(Meal.id == meal_id, Meal.author_id == user.id))
        result = await session.execute(stmt)
        user = result.unique().scalar_one()
        return user
    except NoResultFound:
        raise HTTPException(status.HTTP_403_FORBIDDEN)