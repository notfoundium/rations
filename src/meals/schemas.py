from pydantic import BaseModel, ConfigDict


class Allergen(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    name: str


class MealCreate(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    name: str
    description: str = ""

    calories: float
    protein: float
    fat: float
    carbohydrate: float


class MealRead(MealCreate):
    id: str
    author_id: str


class Meal(MealRead):
    ingredients: list[MealRead] = []
    allergens: list[Allergen] = []
