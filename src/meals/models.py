from sqlalchemy import String, Column, ForeignKey, Table, Float
from sqlalchemy.orm import relationship

from src.database import Base
from src.utils import generate_id


meal_allergens = Table(
    "meal_allergens",
    Base.metadata,
    Column("meal_id", ForeignKey("meals.id")),
    Column("allergen_id", ForeignKey("allergens.id")),
)

meal_ingredients = Table(
    "meal_ingredients",
    Base.metadata,
    Column("meal_id", ForeignKey("meals.id")),
    Column("ingredient_id", ForeignKey("meals.id"))
)


class Allergen(Base):
    __tablename__ = "allergens"

    id = Column("id", String, nullable=False, primary_key=True, default=generate_id)
    name = Column("name", String, nullable=False)


class Meal(Base):
    __tablename__ = "meals"

    id = Column("id", String, nullable=False, primary_key=True, default=generate_id)
    name = Column("name", String, nullable=False)
    description = Column("description", String, nullable=False, default="")
    calories = Column("calories", Float, nullable=False, default="0")
    protein = Column("protein", Float, nullable=False, default="0")
    fat = Column("fat", Float, nullable=False, default="0")
    carbohydrate = Column("carbohydrate", Float, nullable=False, default="0")
    author_id = Column("author_id", ForeignKey("users.id", ondelete="CASCADE"), nullable=False)

    ingredients = relationship(
        "Meal",
        secondary=meal_ingredients,
        primaryjoin=id==meal_ingredients.c.meal_id,
        secondaryjoin=id==meal_ingredients.c.ingredient_id,
        lazy="selectin"
    )
    allergens = relationship(Allergen, secondary=meal_allergens, lazy="joined")
