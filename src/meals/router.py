from fastapi import Depends, HTTPException, APIRouter, status
from sqlalchemy import select, or_
from sqlalchemy.orm import selectinload
from sqlalchemy.exc import NoResultFound

from src.database import get_session, AsyncSession

from src.users.dependencies import current_user
from src.users.models import User

from .dependencies import meal_author, valid_allergen
from .models import Allergen, Meal
from .schemas import (
    Allergen as AllergenSchema,
    MealCreate,
    MealRead,
    Meal as MealSchema
)


router = APIRouter()


@router.post(
    "/allergens",
    status_code=status.HTTP_201_CREATED,
    response_model=AllergenSchema,
    dependencies=[
        Depends(current_user)
    ]
)
async def create_allergen(
        create_data: AllergenSchema,
        session: AsyncSession = Depends(get_session)
):
    result = await session.execute(select(Allergen).where(Allergen.name == create_data.name))
    if result.scalar_one_or_none() is not None:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Allergen already exists")
    allergen = Allergen(name=create_data.name.lower())
    session.add(allergen)
    await session.commit()
    return allergen


@router.get(
    "/allergens",
    status_code=status.HTTP_200_OK,
    response_model=list[AllergenSchema]
)
async def get_allergens(
        session: AsyncSession = Depends(get_session)
):
    result = await session.execute(select(Allergen))
    return result.scalars()


@router.post(
    "/meals",
    status_code=status.HTTP_201_CREATED,
    response_model=MealRead
)
async def create_meal(
        create_data: MealCreate,
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    create_data_dict = create_data.model_dump()
    create_data_dict["author_id"] = user.id
    meal = Meal(**create_data_dict)
    session.add(meal)
    await session.commit()
    await session.refresh(meal)
    return meal


@router.get(
    "/meals",
    status_code=status.HTTP_201_CREATED,
    response_model=list[MealSchema]
)
async def get_my_meals(
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    stmt = select(Meal).where(Meal.author_id == user.id).options(selectinload(Meal.ingredients))
    result = await session.scalars(stmt)
    return result.unique().all()


@router.patch(
    "/meals/{meal_id}/add_ingredient/{ingredient_id}",
    status_code=status.HTTP_200_OK,
    response_model=MealSchema,
    dependencies=[Depends(meal_author)]
)
async def add_ingredient(
        meal_id: str,
        ingredient_id: str,
        session: AsyncSession = Depends(get_session)
):
    try:
        result = await session.scalars(select(Meal).where(Meal.id == meal_id).options(selectinload(Meal.ingredients)))
        meal = result.unique().one()
        result = await session.scalars(select(Meal).where(Meal.id == ingredient_id).options(selectinload(Meal.ingredients)))
        ingredient = result.unique().one()
    except NoResultFound:
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Invalid Meal ID")
    meal.ingredients.append(ingredient)
    session.add(meal)
    await session.commit()
    await session.refresh(meal)
    return meal


@router.patch(
    "/allergens/{allergen_name}/add_to_user"
)
async def add_allergen_to_user(
        allergen_name: str,
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    try:
        result = await session.execute(select(Allergen).where(Allergen.name == allergen_name))
        allergen = result.unique().scalar_one()
    except NoResultFound:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Invalid allergen")
    user.allergens.append(allergen)
    session.add(user)
    await session.commit()
    await session.refresh(user)
    return user


@router.patch(
    "/allergens/{allergen_name}/add_to_meal/{meal_id}",
    status_code=status.HTTP_200_OK,
    response_model=MealSchema,
    dependencies=[Depends(meal_author)]
)
async def add_allergen_to_meal(
        meal_id: str,
        allergen: Allergen = Depends(valid_allergen),
        session: AsyncSession = Depends(get_session)
):
    try:
        result = await session.execute(select(Meal).where(Meal.id == meal_id))
        meal = result.unique().scalar_one()
    except NoResultFound:
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Invalid Meal ID")
    meal.allergens.append(allergen)
    session.add(meal)
    await session.commit()
    await session.refresh(meal)
    return meal


@router.get(
    "/meals/search",
    status_code=status.HTTP_200_OK,
    response_model=list[MealRead]
)
async def search_meal(
        q: str,
        session: AsyncSession = Depends(get_session)
):
    stmt = select(Meal).where(or_(Meal.name.ilike(f"%{q}%"), Meal.description.ilike(f"%{q}%")))
    result = await session.execute(stmt)
    return result.unique().scalars()
