from pydantic import BaseModel, ConfigDict

from src.meals.schemas import Allergen


class Login(BaseModel):
    email: str
    password: str


class UserCreateRequest(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    name: str
    email: str
    password: str


class UserRead(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: str
    name: str
    email: str

    allergens: list[Allergen]


class AuthResponse(BaseModel):
    access_token: str
    token_type: str = "bearer"
