from sqlalchemy import String, Column, ForeignKey, Table, Boolean
from sqlalchemy.orm import relationship

from src.database import Base
from src.utils import generate_id

from src.meals.models import Allergen


user_allergens = Table(
    "user_allergens",
    Base.metadata,
    Column("user_id", ForeignKey("users.id")),
    Column("allergen_id", ForeignKey("allergens.id")),
)


class User(Base):
    __tablename__ = "users"

    id = Column("id", String, nullable=False, primary_key=True, default=generate_id)
    name = Column("name", String, nullable=False)
    email = Column("email", String, nullable=False, unique=True)
    hashed_password = Column("hashed_password", String, nullable=False)
    is_superuser = Column("is_superuser", Boolean, nullable=False, default=False)

    allergens = relationship(Allergen, secondary=user_allergens, lazy="joined")


class Session(Base):
    __tablename__ = "sessions"

    id = Column("id", String, nullable=False, primary_key=True, default=generate_id)
    user_id = Column("user_id", ForeignKey("users.id", ondelete="CASCADE"), nullable=False)
