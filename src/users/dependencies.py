from typing import Annotated

from fastapi import Depends, HTTPException, status, Header
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from src.database import get_session, AsyncSession

from .models import User, Session


async def auth_token(
        Authorization: Annotated[str, Header()]
):
    return Authorization.split(" ")[-1]


async def current_user(
        token: str = Depends(auth_token),
        session: AsyncSession = Depends(get_session)
):
    try:
        stmt = select(User).where(
            User.id == (select(Session.user_id).where(Session.id == token)).scalar_subquery()
        )
        result = await session.execute(stmt)
        user = result.unique().scalar_one()
        return user
    except NoResultFound:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED)
