from bcrypt import gensalt, hashpw, checkpw
from fastapi import Depends, HTTPException, APIRouter, status
from sqlalchemy import select, or_
from sqlalchemy.exc import NoResultFound

from src.database import get_session, AsyncSession

from .dependencies import auth_token
from .models import User, Session
from .schemas import UserCreateRequest, UserRead, Login, AuthResponse


router = APIRouter()


@router.post(
    "/register",
    status_code=status.HTTP_201_CREATED,
    response_model=UserRead
)
async def register(
        create_data: UserCreateRequest,
        session: AsyncSession = Depends(get_session)
):
    stmt = select(User).where(User.email == create_data.email)
    result = await session.execute(stmt)
    if result.first() is not None:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "User with given email already exists")
    create_data_dict = create_data.model_dump()
    create_data_dict["hashed_password"] = hashpw(create_data.password.encode("utf-8"), gensalt()).decode("utf-8")
    create_data_dict.pop("password")
    user = User(**create_data_dict)
    session.add(user)
    await session.commit()
    await session.refresh(user)
    return user


@router.post(
    "/signin",
    status_code=status.HTTP_200_OK,
    response_model=AuthResponse
)
async def signin(
        login_data: Login,
        session: AsyncSession = Depends(get_session)
):
    stmt = select(User).where(User.email == login_data.email)
    try:
        result = await session.execute(stmt)
        user = result.unique().scalar_one()
    except NoResultFound:
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Incorrect email or password")
    if checkpw(login_data.password.encode("utf-8"), bytes(user.hashed_password, "utf-8")):
        auth_session = Session(user_id=user.id)
        session.add(auth_session)
        await session.commit()
        await session.refresh(auth_session)
        return AuthResponse(access_token=auth_session.id)
    else:
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Incorrect email or password")


@router.post(
    "/signout",
    status_code=status.HTTP_200_OK
)
async def signout(
        token: str = Depends(auth_token),
        session: AsyncSession = Depends(get_session)
):
    try:
        stmt = select(Session).where(Session.id == token)
        result = await session.execute(stmt)
        user_session = result.scalar_one()
    except NoResultFound:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED)
    await session.delete(user_session)
    await session.commit()


@router.get(
    "/users/search",
    status_code=status.HTTP_200_OK,
    response_model=list[UserRead]
)
async def search_user(
        q: str,
        session: AsyncSession = Depends(get_session)
):
    stmt = select(User).where(or_(User.name.ilike(f"%{q}%"), User.email.ilike(f"%{q}%")))
    result = await session.execute(stmt)
    return result.unique().scalars()


@router.get(
    "/users/{id}",
    status_code=status.HTTP_200_OK,
    response_model=UserRead
)
async def get_user(
        id: str,
        session: AsyncSession = Depends(get_session)
):
    result = await session.execute(select(User).where(User.id == id))
    user = result.unique().scalar_one_or_none()
    if user:
        return user
    raise HTTPException(status.HTTP_404_NOT_FOUND)
