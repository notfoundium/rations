from fastapi import Depends, HTTPException, status
from sqlalchemy import select, and_
from sqlalchemy.exc import NoResultFound

from src.database import get_session, AsyncSession

from src.users.models import User
from src.users.dependencies import current_user

from .models import Ration


async def ration_author(
        ration_id: str,
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    try:
        stmt = select(Ration).where(and_(Ration.id == ration_id, Ration.author_id == user.id))
        result = await session.execute(stmt)
        user = result.unique().scalar_one()
        return user
    except NoResultFound:
        raise HTTPException(status.HTTP_403_FORBIDDEN)
