from pydantic import BaseModel, ConfigDict

from src.meals.schemas import MealRead


class RationCreate(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    name: str
    description: str = ""


class RationRead(RationCreate):
    id: str
    author_id: str

    calories: float
    protein: float
    fat: float
    carbohydrate: float


class Ration(RationRead):
    meals: list[MealRead]
