from sqlalchemy import String, Column, ForeignKey, Float, Table
from sqlalchemy.orm import relationship

from src.database import Base
from src.utils import generate_id

from src.meals.models import Meal


ration_meals = Table(
    "ration_meals",
    Base.metadata,
    Column("ration_id", ForeignKey("rations.id")),
    Column("meal_id", ForeignKey("meals.id")),
)


class Ration(Base):
    __tablename__ = "rations"

    id = Column("id", String, nullable=False, primary_key=True, default=generate_id)
    name = Column("name", String, nullable=False)
    description = Column("description", String, nullable=False, default="")
    calories = Column("calories", Float, nullable=False, default=0)
    protein = Column("protein", Float, nullable=False, default=0)
    fat = Column("fat", Float, nullable=False, default=0)
    carbohydrate = Column("carbohydrate", Float, nullable=False, default=0)
    author_id = Column("author_id", ForeignKey("users.id", ondelete="CASCADE"), nullable=False)

    meals = relationship(Meal, secondary=ration_meals, lazy="selectin")
