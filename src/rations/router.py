from fastapi import Depends, HTTPException, APIRouter, status
from sqlalchemy import select, or_
from sqlalchemy.exc import NoResultFound

from src.database import get_session, AsyncSession

from src.users.dependencies import current_user
from src.users.models import User
from src.meals.models import Meal

from .dependencies import ration_author
from .models import Ration
from .schemas import RationCreate, RationRead, Ration as RationSchema


router = APIRouter()


@router.post(
    "/rations",
    status_code=status.HTTP_201_CREATED,
    response_model=RationRead,
    dependencies=[
        Depends(current_user)
    ]
)
async def create_ration(
        create_data: RationCreate,
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    create_data_dict = create_data.model_dump()
    create_data_dict["author_id"] = user.id
    ration = Ration(**create_data_dict)
    session.add(ration)
    await session.commit()
    await session.refresh(ration)
    return ration


@router.get(
    "/rations",
    status_code=status.HTTP_200_OK,
    response_model=list[RationSchema]
)
async def get_my_rations(
        user: User = Depends(current_user),
        session: AsyncSession = Depends(get_session)
):
    stmt = select(Ration).where(Ration.author_id == user.id)
    result = await session.scalars(stmt)
    return result.unique().all()


@router.patch(
    "/rations/{ration_id}/add/{meal_id}",
    status_code=status.HTTP_200_OK,
    response_model=RationSchema,
    dependencies=[Depends(ration_author)]
)
async def add_meal(
        ration_id: str,
        meal_id: str,
        session: AsyncSession = Depends(get_session)
):
    try:
        stmt = select(Ration).where(Ration.id == ration_id)
        result = await session.execute(stmt)
        ration = result.unique().scalar_one()
        stmt = select(Meal).where(Meal.id == meal_id)
        result = await session.execute(stmt)
        meal = result.unique().scalar_one()
    except NoResultFound:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Invalid Ration or Meal ID")
    ration.meals.append(meal)
    session.add(ration)
    await session.commit()
    await session.refresh(ration)
    return ration


@router.get(
    "/rations/search",
    status_code=status.HTTP_200_OK,
    response_model=list[RationRead]
)
async def search_ration(
        q: str,
        session: AsyncSession = Depends(get_session)
):
    stmt = select(Ration).where(or_(Ration.name.ilike(f"%{q}%"), Ration.description.ilike(f"%{q}%")))
    result = await session.execute(stmt)
    return result.unique().scalars()
