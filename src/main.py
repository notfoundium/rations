from fastapi import FastAPI

from src.users.router import router as users_router
from src.meals.router import router as meals_router
from src.rations.router import router as rations_router
from src.programs.router import router as programs_router


app = FastAPI()

app.include_router(users_router)
app.include_router(meals_router)
app.include_router(rations_router)
app.include_router(programs_router)
